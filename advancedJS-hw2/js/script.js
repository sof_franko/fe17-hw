//  Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.
// При возникновении ошибки в коде он перестет выполнятся - сам скрипт "падает", выводя в консоль ошибку. Участок кода, в котором вероятно может случиться ошибка, с помощью try...catch её можно зафиксировать, тем самым не прекратив выполнение.

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const booksList = document.createElement("ul");
const container = document.getElementById('root');

container.append(booksList);

// const allBookProperties = Object.getOwnPropertyNames(books[0]);
// console.log(allBookProperties)

books.forEach((book, index) => {
    if (Object.keys(book).includes('name') && Object.keys(book).includes('author') && Object.keys(book).includes('price')) {
        container.insertAdjacentHTML("beforeend", `<li><ul>
    <li>name:${book.name}</li>
    <li>author:${book.author}</li>
    <li>price:${book.price}</li></ul></li>`);
    }
        try {
            if (!book.hasOwnProperty('name')) {
                    throw new Error(`property for object ${index+1} name is not defined`)
                } else if (!book.hasOwnProperty('author')) {
                    throw new Error(`property for object ${index+1} author is not defined`)
                } else if (!book.hasOwnProperty('price')) {
                    throw new Error(`property for object ${index+1} price is not defined`)
                }

        } catch (e) {
            console.error(`${e.name}: ${e.message}`);
        }

})