const { src, dest, task, series, watch } = require('gulp');
const concat = require('gulp-concat');
const clean = require('gulp-clean');
const cleanCSS = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');
const sass = require('gulp-sass');
const terser = require('gulp-terser-js');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();

const path = {
    src: {
        html: 'src/index.html',
        styles: 'src/scss/**/*.scss',
        js: 'src/js/*.js',
        img: 'src/img/**/*.+(png|jpg|gif|jpeg)'
    },

    dist: {
        root: 'dist/',
        styles: 'dist/css/',
        js: 'dist/js',
        img: 'dist/img',
    }
}

const cleanDist = () => (
    src(path.dist.root, {allowEmpty: true})
        .pipe(clean())
);

const buildHTML = () => (
    src(path.src.html)
        .pipe(dest(path.dist.root))
);

const buildCSS = () => (
    src(path.src.styles)
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('styles.min.css'))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(dest(path.dist.styles))
)

const buildImg = () => (
    src(path.src.img)
        .pipe(imagemin())
        .pipe(dest(path.dist.img))
);

const buildJS = () => (
    src(path.src.js)
        .pipe(concat('scripts.min.js'))
        .pipe(terser({
            mangle: {
                toplevel: true
            }
        }))
        .on('error', function (error) {
            this.emit('end')
        })
        .pipe(dest(path.dist.js))
);

const watcher = () => {
    browserSync.init({
        server: {
            baseDir: './dist'
        }
    });

    watch(path.src.html, buildHTML).on('change',browserSync.reload);
    watch(path.src.styles, buildCSS).on('change',browserSync.reload);
    watch(path.src.js, buildJS).on('change',browserSync.reload);
}

task('build', series(
    cleanDist,
    buildHTML,
    buildCSS,
    buildImg,
    buildJS
));

task('default', series(
    'build',
    watcher
));
