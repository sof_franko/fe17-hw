function renderBurgermenu() {
    const burger = document.querySelector('.header__burger-content');
    const menu = document.querySelector('.burgermenu');

    burger.onclick = function () {
        burger.classList.toggle('burger-toggle');
        menu.classList.toggle('hidden');
    }
}

renderBurgermenu()