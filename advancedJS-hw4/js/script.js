//  Обьясните своими словами, что такое AJAX и чем он полезен при разработке на Javascript.

// AJAX - свойство в JS предусмотреное для сетевых запросов (запрашивая/отправляя информацию из кода/в код на сервер). Самый широкораспространенный метод - fetch.


class Film {
    constructor({episode_id, title, opening_crawl, characters}) {
        this.episode_id = episode_id;
        this.title = title;
        this.opening_crawl = opening_crawl;
        this.characters = characters;

        this.elements = {
            film: document.createElement('div'),
            episode_id: document.createElement('h4'),
            title: document.createElement('h2'),
            opening_crawl: document.createElement('p'),
            characters: document.createElement('ul')
        }

        this.elements.episode_id.textContent = this.episode_id;
        this.elements.title.textContent = this.title;
        this.elements.opening_crawl.textContent = this.opening_crawl;

        const allCharacters = this.characters.map(character => `<li>${character}</li>`).join('');
        this.elements.characters.innerHTML = allCharacters;

        this.elements.film.style.paddingBottom = '30px';
        this.elements.characters.style.fontSize = '18px';
    }

    render(container) {
        const {film, episode_id, title, opening_crawl, characters} = this.elements;
        title.append(characters);
        film.append(episode_id, title, opening_crawl);
        container.append(film)
    }
}


fetch('https://swapi.dev/api/films/', {
    method: 'GET'
})
    .then(res => res.json())
    .then(data => {
        const filmsList = data.results.map(film => new Film(film));
        console.log(filmsList);

        const filmsContainer = document.createElement('div');
        filmsList.forEach(film => film.render(filmsContainer));
        document.body.prepend((filmsContainer))
    })
