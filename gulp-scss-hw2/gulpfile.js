const gulp = require('gulp');
const clean = require('gulp-clean');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const imgMin = require('gulp-imagemin');
const newer = require('gulp-newer');
const cleanCss = require('gulp-clean-css');
const minify = require('gulp-js-minify');
// const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync');

const paths = {
    dev: {
        css: 'src/scss/**/*.scss',
        js: 'src/js/**/*.js',
        img: 'src/img/*.+(png|jpg|gif|jpeg)',
        html: 'src/index.html'
    },
    build: {
        html: 'dist',
        css: 'dist/css',
        js: 'dist/js',
        img: 'dist/img'
    }
}

const buildClean = () => (
    gulp.src(paths.build.html, {allowEmpty: true})
        .pipe(clean())
);

const buildHTML = () => (
    gulp.src(paths.dev.html)
        .pipe(gulp.dest(paths.build.html))
);

const buildCSS = () => (
    gulp.src(paths.dev.css)
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCss({compatibility: 'ie7'}))
        .pipe(concat('styles.min.css'))
        .pipe(gulp.dest(paths.build.css))
);

const buildJS = () => (
    gulp.src(paths.dev.js)
        .pipe(minify({
            ext: {
                min: '.min.js'
            },
            ignoreFiles: ['-min.js']
        }))
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest(paths.build.js))
);

const imageMin = () => (
    gulp.src(paths.dev.img)
            .pipe(newer(paths.build.img))
            .pipe(imgMin({
                interlaced: true,
                progressive: true,
                optimizationLevel: 5,
            }))
            .pipe(gulp.dest(paths.build.img))
);


const watcher = () => {
    browserSync.init({
        server: {
            baseDir: paths.build.html
        }
    })
    gulp.watch(paths.dev.html, buildClean).on('change', browserSync.reload);
    gulp.watch(paths.dev.html, buildHTML).on('change', browserSync.reload);
    gulp.watch(paths.dev.css, buildCSS).on('change', browserSync.reload);
    gulp.watch(paths.dev.js, buildJS).on('change', browserSync.reload);
    gulp.watch(paths.dev.img, imageMin).on('change', browserSync.reload);

}


gulp.task('default', gulp.series(
    buildClean,
    buildHTML,
    buildCSS,
    buildJS,
    imageMin,
    watcher
))