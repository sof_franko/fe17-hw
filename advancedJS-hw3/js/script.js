//  Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript
// Само по себе прототипное наследование это возможность передачи информации в виде методов и свойств от одного объекта другому(от "родителя" к "ребенку"), и использования ее в контексте аргументов наследуемого объекта

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary
    }

    set name(name) {
        this._name = name;
    }
    get name() {
        return this._name
    }

    set age(age) {
        this._age = age;
    }
    get age() {
        return this._age
    }

    set salary(salary) {
        this._salary = salary;
    }
    get salary() {
        return this._salary
    }

}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.language = lang
    }

    get salary() {
        return super.salary * 3;
    }

    set salary(salary) {
        super.salary = salary
    }
}

const programmer1 = new Programmer("Anton", 19, 34.44, "Italian");
console.log(programmer1);
console.log(programmer1.salary);

const programmer2 = new Programmer("Katrin", 32, 50, "French");
console.log(programmer2);
console.log(programmer2.salary);

const programmer3 = new Programmer("Hujo", 25, 78, "Japanese");
console.log(programmer3);
console.log(programmer3.salary);



